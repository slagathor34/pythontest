from fastapi import FastAPI
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
from opentelemetry import trace
import time

tracer = trace.get_tracer(__name__)

app = FastAPI()

@app.get("/")
async def read_root():
    with tracer.start_as_current_span("blue-span"):
      return {"message": "Hello, Blue World", "version": "v1"}

FastAPIInstrumentor.instrument_app(app)