# GitOps Workflow with FluxCD, Bitbucket Repository, Azure AKS
```mermaid
sequenceDiagram
    participant Developer
    participant Jira
    participant Bitbucket
    participant TestEnvironment
    participant FluxCD
    participant AzureAKS
    participant Teams
    Developer->>Jira: Create Jira ticket
    Jira-->>Developer: Assign ticket
    Developer->>Bitbucket: Create feature branch and commit changes
    Developer->>Bitbucket: Create PR linked to Jira ticket
    Bitbucket->>TestEnvironment: Run tests
    TestEnvironment-->>Developer: Return test results
    Developer->>Bitbucket: Merge PR to master (if tests pass and PR is approved)
    Bitbucket->>FluxCD: Trigger on merge to master
    FluxCD->>Bitbucket: Pull latest changes from master
    FluxCD->>AzureAKS: Deploy to Dev/Staging
    AzureAKS->>TestEnvironment: Run tests in Dev/Staging
    TestEnvironment-->>FluxCD: Return test results
    FluxCD->>AzureAKS: Deploy to Production (if tests pass)
    AzureAKS-->>FluxCD: Return deployment status
    FluxCD->>Teams: Notify of deployment status via Webhook
```
## Step by step process
1. The developer creates a feature branch and commits changes to Bitbucket.
1. The developer creates a Pull Request (PR) in Bitbucket.
1. Automated tests are run against the PR in a test environment.
1. If the tests pass, the PR is reviewed.
1. If the PR is approved, it is merged into the master branch.
1. The merge triggers FluxCD.
1. FluxCD pulls the latest changes from the master branch in Bitbucket.
1. FluxCD deploys the changes to a lower environment (Dev/Staging) in Azure AKS.
1. Tests are run in the lower environment.
1. If the tests pass, the changes are deployed to the production environment.
1. The status of the deployment is reported back to FluxCD.
1. FluxCD notifies the developer of the status.
