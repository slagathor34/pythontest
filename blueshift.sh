#!/bin/bash 

case $1 in
    $PATTERN1)
        echo "Deploying Blueshift Pattern #1"
        blueshift job_template launch --id $WORKFLOW_ID1 --monitor
        ;;
    $PATTERN2)
        echo "Deploying Blueshift Pattern #2"
        blueshift job_template launch --id $WORKFLOW_ID2 --monitor
        ;;
    $PATTERN3)
        echo "Deploying Blueshift Pattern #3"
        blueshift job_template launch --id $WORKFLOW_ID3 --monitor
        ;;
    $PATTERN4)
        echo "Deploying Blueshift Pattern #4"
        blueshift job_template launch --id $WORKFLOW_ID4 --monitor
        ;;
    $PATTERN5)
        echo "Deploying Blueshift Pattern #5"
        blueshift job_template launch --id $WORKFLOW_ID5 --monitor
        ;;
    *)
        echo "Invalid pattern. Please use one of the following: $PATTERN1, $PATTERN2, $PATTERN3, $PATTERN4, $PATTERN5"
esac