```
wget https://raw.githubusercontent.com/istio/istio/release-1.17/samples/addons/jaeger.yaml -O aks-jaeger.yaml
```

```
sed -i 's/istio-system/aks-istio-system/g' aks-jaeger.yaml
```

```kubectl apply -f aks-jaeger.yaml
deployment.apps/jaeger created
service/tracing created
service/zipkin created
service/jaeger-collector created
```

```
istioctl -i aks-istio-system dashboard jaeger -n aks-istio-system
```

```mermaid
flowchart LR
  A[Developer]
  B[Bitbucket]
  C[SonarQube]
  D[Dynamic Code Analysis - None]
  E[Dependency Scanning - None]
  F[Container Scanning - None]
  G[Secrets Detection - None]
  H[Threat Modeling - Wiz]
  I[Jenkins]
  J[Flossum]
  K[Mac Stadium]
  L[Artifactory]

  A --> B;
  B --> C;
  C --> D;
  D --> E;
  E --> F;
  F --> G;
  G --> H;
  H --> I; 
  I --> J;
  J --> K;
  K --> L;

```