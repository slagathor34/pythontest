# pythontest

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=brainstormes-consulting_awx-lab&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=brainstormes-consulting_awx-lab)

## Set Azure Subscription ID
```
az account set --subscription <subscription_id>
```
## Get AKS credentials for kubectl login
```
az aks get-credentials --resource-group <RESOURCE_GROUP> --name <AKS_CLUSTER_NAME>
```
## Add aks-preview extensions to the subscription
```
az extension add --name aks-preview
```
## Register the Azure Istio Service Mesh Extension
```
az feature register --namespace "Microsoft.ContainerService" --name "AzureServiceMeshPreview"
```
## Check on Registration Status
```
az feature show --namespace "Microsoft.ContainerService" --name "AzureServiceMeshPreview"
```
## Add Istio Service Mesh to the AKS Cluster
```
az aks mesh enable --resource-group <RESOURCE_GROUP> --name <AKS_CLUSTER_NAME>
```
## Attach the AKS cluster to the Azure Container Registry
```
az aks update -n awxcluster01 -g <RESOURCE_GROUP> --attach-acr <ACR_NAME>
```
## Create an Istio Ingress Controller to the AKS Cluster
```
az aks mesh enable-ingress-gateway --resource-group <RESOURCE_GROUP> --name <AKS_CLUSTER_NAME> --ingress-gateway-type external
 ```
