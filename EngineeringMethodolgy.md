# Engineering Process
1. **Observation**: The team identifies a problem or a need. This could be a software bug, a system inefficiency, or a new feature request. Number of reported issues, system downtime, etc.

1. **Question**: The team formulates a question that the work will answer. For example, “How can we improve the performance of this algorithm?” or “How can we fix this bug?” Clearly defined success metrics (e.g., reduced latency, fewer bugs, etc.)

1. **Hypothesis**: The team proposes a solution to the problem. This could be a new algorithm, a code fix, or a design change. Estimated impact of the proposed solution (e.g., 20% performance improvement, etc.)

1. **Prediction**: The team predicts the outcome of the proposed solution. This could involve estimating the improved performance or the expected behavior after a bug fix. Benchmarks for the expected outcome (e.g., load time under 3 seconds, etc.)

1. **Testing**: The team implements the solution and tests it in a controlled environment. This could involve unit testing, integration testing, or user acceptance testing. Data collected during testing (e.g., actual load time, number of passed/failed tests, etc.)

1. **Analysis**: The team analyzes the test results. If the results align with the predictions, the solution may be considered valid. If not, the team will need to revise the hypothesis or consider alternative solutions. Comparison of test results with benchmarks (e.g., was load time under 3 seconds? etc.)

1. **Iteration**: The team repeats the process until a satisfactory solution is found. In IT, this often involves multiple iterations, as initial solutions may not fully solve the problem or may introduce new issues.

1. **Documentation**: Once a satisfactory solution is implemented, the team documents their findings for future reference. This helps build a knowledge base and can speed up future troubleshooting. Record of metrics for future reference (e.g., final load time, number of iterations, etc.)

```mermaid
graph TD
    A[Observation: Identify problem or need]
    B[Question: Formulate question, define success metrics]
    C[Hypothesis: Propose solution, estimate impact]
    D[Prediction: Predict outcome, set benchmarks]
    E[Testing: Implement & test solution, collect data]
    F{Analysis: Analyze test results, compare with benchmarks}
    G[Documentation: Document findings, record metrics]
    H[Iteration: If needed, revise hypothesis or consider alternatives]
    A --> B
    B --> C
    C --> D
    D --> E
    E --> F
    F -->|If results align with predictions| G
    F -->|If results do not align| H
    H --> C
    G --> I[End of Process]
```

## Observation
1. **Identify Problem**: This is where the team first notices an issue. It could be a bug reported by a user, a system crash, a slow-running query, or any other problem that needs addressing.

1. **Collect Data**: Once the problem is identified, the team collects as much data as possible about it. This could include error logs, system metrics, user feedback, and any other relevant information.

1. **Define Scope**: The team determines the extent of the problem. How many users are affected? How often does the problem occur? What parts of the system are involved? This helps the team understand the impact of the problem and prioritize their efforts.

1. **Record Observations**: The team documents their observations, including the nature of the problem, the data collected, and the defined scope. This record can be referred back to throughout the problem-solving process and can also be useful for future troubleshooting.

```mermaid
graph TD
    A[Identify Problem: Detect an issue or inefficiency in the system]
    B[Collect Data: Gather relevant data about the problem]
    C[Define Scope: Determine the extent and impact of the problem]
    D[Record Observations: Document the problem and associated data for future reference]
    A --> B
    B --> C
    C --> D
```

## Question 
1. **Define Problem Statement**: Based on the observations, the team clearly articulates the problem. This statement should be specific, measurable, achievable, relevant, and time-bound (SMART).

1. **Formulate Questions**: The team develops specific questions that need to be answered to solve the problem. These questions guide the investigation and solution development.

1. **Define Success Metrics**: The team determines what success looks like in measurable terms. These could be Key Performance Indicators (KPIs) like reduced latency, increased throughput, fewer bugs, etc.

1. **Record Questions**: The team documents the problem statement, questions, and success metrics. This record provides a reference point throughout the problem-solving process and can be useful for future troubleshooting.

```mermaid
graph TD
    A[Identify Problem: Detect an issue or inefficiency in the system]
    B[Collect Data: Gather relevant data about the problem]
    C[Define Scope: Determine the extent and impact of the problem]
    D[Record Observations: Document the problem and associated data for future reference]
    A --> B
    B --> C
    C --> D
```

## Hypothesis
1. **Propose Solution**: Based on the observations and questions, the team develops a potential solution. This could be a new feature, a bug fix, a system improvement, etc.

1. **Estimate Impact**: The team predicts the potential impact of the proposed solution. This could be improved performance, reduced errors, increased user satisfaction, etc.

1. **Define Test Criteria**: The team determines how the solution will be tested. This could involve defining specific test cases, identifying expected outcomes, setting up a testing environment, etc.

1. **Record Hypothesis**: The team documents the proposed solution, estimated impact, and test criteria. This record provides a reference point throughout the problem-solving process and can be useful for future troubleshooting.

```mermaid
graph TD
    A[Propose Solution: Develop a potential solution based on observations and questions]
    B[Estimate Impact: Predict the potential impact of the proposed solution]
    C[Define Test Criteria: Determine how the solution will be tested]
    D[Record Hypothesis: Document the proposed solution, estimated impact, and test criteria for future reference]
    A --> B
    B --> C
    C --> D
```

## Predictions

1. **Define Expected Outcome**: Based on the proposed solution, the team describes the expected result. This could be improved system performance, reduced error rates, increased user satisfaction, etc.

1. **Set Benchmarks**: The team establishes measurable benchmarks that the solution should meet. These could be Key Performance Indicators (KPIs) like response time under 200ms, error rate below 0.1%, uptime of 99.99%, etc.

1. **Estimate Resources**: The team predicts the resources (time, cost, manpower) needed to implement the solution. This could involve estimating the development time, calculating the cost of additional hardware or software, assessing the need for extra personnel, etc.

1. **Record Predictions**: The team documents the expected outcome, benchmarks, and resource estimates. This record provides a reference point throughout the problem-solving process and can be useful for future troubleshooting.

```mermaid
graph TD
    A[Define Expected Outcome: Describe the expected result of the proposed solution]
    B[Set Benchmarks: Establish measurable benchmarks that the solution should meet]
    C[Estimate Resources: Predict the resources - time, cost, manpower - needed to implement the solution]
    D[Record Predictions: Document the expected outcome, benchmarks, and resource estimates for future reference]
    A --> B
    B --> C
    C --> D
```

## Testing

1. **Implement Solution**: Based on the proposed solution, the team develops the solution. This could involve writing new code, configuring systems, setting up infrastructure, etc.

1. **Define Test Cases**: The team establishes specific scenarios to test the solution. These test cases should cover a range of conditions, including edge cases and potential failure points.

1. **Setup Environment**: The team prepares the testing environment. This could involve setting up servers, configuring networks, preparing data, etc. Tools like Ansible and Desired State Configuration (DSC) can be used to automate this process. Ansible can be used to automate the deployment and configuration of systems, while DSC ensures that the systems are in the desired state.

1. **Execute Tests**: The team runs the tests and collects data. This could involve running scripts, monitoring system performance, logging errors, etc.

1. **Record Results**: The team documents the test results. This record provides a reference point for the analysis stage and can be useful for future troubleshooting.

```mermaid
graph TD
    A[Implement Solution: Develop the solution based on the hypothesis]
    B[Define Test Cases: Establish specific scenarios to test the solution]
    C[Setup Environment: Prepare the testing environment using tools like Ansible and DSC]
    D[Execute Tests: Run the tests and collect data]
    E[Record Results: Document the test results for future reference]
    A --> B
    B --> C
    C --> D
    D --> E
```

## Analysis

1. **Review Test Results**: The team examines the data collected during testing. This could involve reviewing logs, error messages, performance metrics, etc.

1. **Compare with Benchmarks**: The team evaluates the test results against the set benchmarks. For example, if the benchmark was a response time under 200ms, did the solution meet this criterion?

1. **Assess Resource Usage**: The team analyzes the resources used to implement and test the solution, comparing the actual usage against the estimates made during the Prediction phase. Tools like Ansible can provide valuable data on resource usage.

1. **Identify Issues**: If the solution did not meet the benchmarks or if resource usage was higher than estimated, the team identifies these as issues. They also look for any other problems or areas for improvement. Desired State Configuration (DSC) can be useful here to identify discrepancies between the desired and actual state of the system.

1. **Record Analysis**: The team documents their analysis, including the comparison with benchmarks, resource usage assessment, and identified issues. This record provides a reference point for the next steps in the process and can be useful for future troubleshooting.

```mermaid
graph TD
    A[Review Test Results: Examine the data collected during testing]
    B[Compare with Benchmarks: Evaluate the results against the set benchmarks]
    C[Assess Resource Usage: Analyze the resources used against the estimates]
    D[Identify Issues: Highlight any problems or areas for improvement]
    E[Record Analysis: Document the analysis for future reference]
    A --> B
    B --> C
    C --> D
    D --> E
```

## Iteration
1. **Review Analysis**: The team examines the analysis from the previous step. This includes reviewing the comparison with benchmarks, the assessment of resource usage, and any identified issues.

1. **Revise Hypothesis**: Based on the analysis, the team updates the proposed solution. This could involve modifying the code, adjusting the system configuration, changing the design, etc.

1. **Consider Alternatives**: If the revised hypothesis still doesn’t meet the benchmarks or if resource usage is still too high, the team explores other potential solutions. This could involve considering different algorithms, using different technologies, redesigning the system, etc.

1. **Update Test Criteria**: Based on the new hypothesis, the team adjusts the test scenarios. This could involve adding new test cases, modifying existing ones, or changing the expected outcomes.

1. **Record Iteration**: The team documents the iteration process, including the revised hypothesis, any considered alternatives, and the updated test criteria. This record provides a reference point for the next steps in the process and can be useful for future troubleshooting.

```mermaid
graph TD
    A[Review Analysis: Examine the analysis from the previous step]
    B[Revise Hypothesis: Update the proposed solution based on the analysis]
    C[Consider Alternatives: Explore other potential solutions if necessary]
    D[Update Test Criteria: Adjust the test scenarios based on new hypothesis]
    E[Record Iteration: Document the iteration process for future reference]
    A --> B
    B --> C
    C --> D
    D --> E
```

## Documentation

1. **Summarize Findings**: The team compiles a summary of the problem, the proposed solution, and the results of the testing. This provides a high-level overview of the project.

1. **Detail Process**: The team describes the steps taken to solve the problem, including any iterations and revisions. This provides a clear record of the process and can be useful for future troubleshooting or similar projects.

1. **Record Metrics**: The team documents the benchmarks that were set, the results of the testing, and the resources used. This provides a quantitative record of the project and can help in assessing the effectiveness of the solution.

1. **Note Issues**: The team highlights any remaining issues or areas for improvement. This can guide future work on the project.

1. **Archive Documentation**: The team stores the documentation in a place where it can be easily accessed for future reference. This ensures that the knowledge gained from the project is not lost and can benefit future work.

```mermaid
graph TD
    A[Summarize Findings: Compile a summary of the problem, solution, and results]
    B[Detail Process: Describe the steps taken, including any iterations and revisions]
    C[Record Metrics: Document the benchmarks, test results, and resource usage]
    D[Note Issues: Highlight any remaining issues or areas for improvement]
    E[Archive Documentation: Store the documentation for future reference]
    A --> B
    B --> C
    C --> D
    D --> E
```

# Engineering Methodology Specification Outline

## Overview
Briefly describe the problem and the proposed solution.

## Goals
List the goals of the project.

## Observation
- **Identify Problem**: Describe the problem or need.
- **Collect Data**: Detail the data gathered about the problem.
- **Define Scope**: Define the extent and impact of the problem.
- **Record Observations**: Document the problem and associated data.

## Question
- **Define Problem Statement**: Clearly articulate the problem based on observations.
- **Formulate Questions**: List the specific questions that need to be answered.
- **Define Success Metrics**: Determine measurable indicators of success.
- **Record Questions**: Document the questions and success metrics.

## Hypothesis
- **Propose Solution**: Detail the proposed solution.
- **Estimate Impact**: Predict the potential impact of the solution.
- **Define Test Criteria**: Determine how the solution will be tested.
- **Record Hypothesis**: Document the proposed solution, estimated impact, and test criteria.

## Prediction
- **Define Expected Outcome**: Describe the expected result of the proposed solution.
- **Set Benchmarks**: Establish measurable benchmarks that the solution should meet.
- **Estimate Resources**: Predict the resources (time, cost, manpower) needed to implement the solution.
- **Record Predictions**: Document the expected outcome, benchmarks, and resource estimates.

## Testing
- **Implement Solution**: Detail the steps taken to develop the solution.
- **Define Test Cases**: List the specific scenarios used to test the solution.
- **Setup Environment**: Describe how the testing environment was prepared.
- **Execute Tests**: Explain how the tests were run and what data was collected.
- **Record Results**: Document the test results.

## Analysis
- **Review Test Results**: Examine the data collected during testing.
- **Compare with Benchmarks**: Evaluate the results against the set benchmarks.
- **Assess Resource Usage**: Analyze the resources used against the estimates.
- **Identify Issues**: Highlight any problems or areas for improvement.
- **Record Analysis**: Document the analysis.

## Iteration
- **Review Analysis**: Examine the analysis from the previous step.
- **Revise Hypothesis**: Update the proposed solution based on the analysis.
- **Consider Alternatives**: Explore other potential solutions if necessary.
- **Update Test Criteria**: Adjust the test scenarios based on new hypothesis.
- **Record Iteration**: Document the iteration process.

## Documentation
- **Summarize Findings**: Compile a summary of the problem, solution, and results.
- **Detail Process**: Describe the steps taken, including any iterations and revisions.
- **Record Metrics**: Document the benchmarks, test results, and resource usage.
- **Note Issues**: Highlight any remaining issues or areas for improvement.
- **Archive Documentation**: Store the documentation for future reference.


# Engineering Specification Example - Python Fast API conversion from IaaS to PaaS

## Overview
Investigate the need for autoscaling of a Python FastAPI application currently running on a single VM.

**Title**: “Migrate Python FastAPI Application from VM to Azure Kubernetes Service (AKS)”

**Epic**: Infrastructure Modernization

**Story**:
As a Platform Engineering Team,
We need to migrate our Python FastAPI application from a legacy virtual machine environment to a Kubernetes application hosted on Azure,
So that we can leverage the scalability, resilience, and modern infrastructure capabilities of Azure Kubernetes Service (AKS).

**Acceptance Criteria**:

1. **Assessment of Current Infrastructure**: Thoroughly document the existing application architecture and dependencies in the virtual machine environment.
1. **Containerization of Application**: Convert the FastAPI application into a Docker container, ensuring it includes all necessary dependencies and follows best practices for containerization.
1. **Deployment Strategy**: Develop a detailed plan for deploying the containerized application in AKS, including considerations for service discovery, load balancing, and auto-scaling.
1. **Data Migration Plan**: Create a strategy for migrating any associated data from the VM environment to a suitable Azure data service, ensuring data integrity and minimal downtime.
1. **Security and Compliance**: Ensure the AKS deployment meets all security standards and compliance requirements of BlueShield of CA, including network configurations, identity management, and data protection.
1. **Monitoring and Logging**: Implement comprehensive monitoring and logging solutions to track the performance and health of the application in its new environment.
1. **CI/CD Integration**: Integrate the deployment process with the existing CI/CD pipeline, ensuring seamless and automated updates to the Kubernetes environment.
1. **Documentation and Training**: Provide detailed documentation on the new infrastructure and conduct training sessions for the team to manage and operate the application in AKS.
1. **Performance Testing**: Conduct thorough performance testing to ensure the application maintains or improves its performance benchmarks after migration.
1. **Rollback Plan**: Develop a contingency plan for rolling back to the VM environment in case of unforeseen issues during or after the migration.

## Goals
- Improve application performance during peak usage
- Ensure high availability of the application
- Optimize resource usage

## Observation
- **Identify Problem**: The application experiences performance issues during peak usage times.
  - The team has noticed that the current deployment of the Python FastAPI application on a single Virtual Machine is not able to efficiently handle varying loads. During peak usage times, the application’s performance degrades, leading to longer response times and a poor user experience.
- **Collect Data**: Usage data shows a significant increase in traffic during certain hours.
  - The team collects data related to the application’s performance. This includes server logs, error messages, response times, CPU usage, memory usage, network latency, and user feedback. They also gather data on the times when the load peaks occur.
- **Define Scope**: The performance issues affect all users during peak times.
  - The team determines that the performance degradation affects all users of the application during peak times. They also note that the current setup does not allow for easy scaling of resources, which could help manage the varying loads.
- **Record Observations**: Documented the problem and associated data.
  - The team documents their observations, noting the specifics of the performance issues, the data collected, and the defined scope of the problem. They also record their initial thoughts on potential solutions, including the possibility of moving to a cloud-native setup using Kubernetes, which could provide autoscaling capabilities to handle peak loads more efficiently. 

## Question
- **Define Problem Statement**: How can we implement autoscaling to improve the performance of our Python FastAPI application during peak usage times?
  - The team clearly articulates the problem based on their observations: “How can we improve the performance and scalability of our Python FastAPI application currently running on a single VM during peak usage times?”
- **Formulate Questions**: What autoscaling solutions are available? How will they impact performance and cost?
  - The team develops specific questions that need to be answered to solve the problem. These could include:
      - What are the benefits and drawbacks of converting our application to a cloud-native setup using Kubernetes?
      - How would the conversion impact our application’s performance, scalability, and cost?
      - What changes would need to be made to our application’s codebase to make it compatible with Kubernetes?
      - What resources (time, cost, manpower) would be required for the conversion?
      - How would the conversion affect our development, testing, and deployment processes?
- **Define Success Metrics**: Reduced response time during peak usage, no downtime, cost within budget.
  - The team determines what success looks like in measurable terms. These could include:
      - Improved response time during peak usage
      - Ability to automatically scale resources up and down based on demand
      - No downtime during the scaling process
      - Cost-effectiveness of the new setup
- **Record Questions**: Documented the questions and success metrics.
  - The team documents the problem statement, questions, and success metrics. This record provides a reference point throughout the problem-solving process and can be useful for future troubleshooting or similar projects.

## Hypothesis
- **Propose Solution**: Implement an autoscaling solution using Kubernetes.
  - Based on their understanding of Kubernetes and its capabilities, the team proposes that converting the Python FastAPI application to run on a Kubernetes cluster could potentially solve the performance issues during peak times. They hypothesize that Kubernetes’ autoscaling capabilities would allow the application to handle increased load by automatically spinning up additional pods as needed.
- **Estimate Impact**: Predict improved performance during peak usage times and slightly increased cost.
  - The team predicts that this solution would lead to improved performance during peak usage times due to the ability to scale resources up and down based on demand. They also anticipate that the conversion would lead to better resource utilization, as idle resources could be scaled down during off-peak times. However, they also acknowledge that the conversion might lead to increased costs due to the need for container orchestration and potentially higher resource usage.
- **Define Test Criteria**: Test the solution under simulated peak load conditions.
  - The team determines that the solution can be tested by simulating peak load conditions and monitoring the application’s performance. They plan to measure response times, error rates, and resource usage during these tests.
- **Record Hypothesis**: Documented the proposed solution, estimated impact, and test criteria.
  - The team documents their proposed solution, estimated impact, and test criteria. This record provides a reference point throughout the problem-solving process and can be useful for future troubleshooting or similar projects.

## Prediction
- **Define Expected Outcome**: The application should handle peak loads without performance degradation.
  - Based on the proposed solution, the team describes the expected result. They predict that the application will be able to handle increased load during peak usage times without any degradation in performance, thanks to Kubernetes’ autoscaling capabilities.
- **Set Benchmarks**: Response time under 200ms even during peak load, uptime of 99.99%.
  - The team establishes measurable benchmarks that the solution should meet. These could include maintaining a response time under 200ms even during peak load, ensuring an uptime of 99.99%, and keeping the cost within the allocated budget.
- **Estimate Resources**: Predict the resources needed to implement and test the solution.
  - The team predicts the resources (time, cost, manpower) needed to implement the solution. This could involve estimating the development time required to modify the application for Kubernetes, calculating the cost of setting up and maintaining a Kubernetes cluster, and assessing the need for additional personnel or training.
- **Record Predictions**: Documented the expected outcome, benchmarks, and resource estimates.
  - The team documents the expected outcome, benchmarks, and resource estimates. This record provides a reference point throughout the problem-solving process and can be useful for future troubleshooting or similar projects.

## Testing
- **Implement Solution**: Set up a Kubernetes cluster and configured it for autoscaling.
  - The team starts by setting up a Kubernetes cluster and configuring it for autoscaling. They then modify the application as necessary to make it compatible with Kubernetes and deploy it on the cluster.
- **Define Test Cases**: Simulated peak load conditions and monitored application performance.
  -  The team establishes specific scenarios to test the solution. These could include simulating peak load conditions, testing the autoscaling functionality by artificially increasing the load, and checking the application’s performance under different load levels.
- **Setup Environment**: Prepared the testing environment using Ansible and DSC.
  - The team prepares the testing environment. This could involve setting up a separate Kubernetes cluster for testing, configuring the network, preparing the test data, etc. Tools like Ansible can be used to automate the setup process, while Desired State Configuration (DSC) can ensure that the systems are in the desired state.
- **Execute Tests**: Ran the tests and collected performance data.
  - The team runs the tests and collects data. This could involve running scripts to simulate load, monitoring the application’s performance using Kubernetes metrics, logging any errors or issues, etc.
- **Record Results**: Documented the test results.
  - The team documents the test results, noting the application’s performance under different load conditions, the effectiveness of the autoscaling, any issues encountered, etc. This record provides a reference point for the analysis stage and can be useful for future troubleshooting.

## Analysis
- **Review Test Results**: Examined the performance data collected during testing.
  - The team examines the data collected during testing. This includes reviewing the application’s performance metrics under different load conditions, the effectiveness of the autoscaling, any errors or issues encountered, etc.
- **Compare with Benchmarks**: Evaluated the test results against the set benchmarks.
  - The team evaluates the test results against the set benchmarks. For example, if the benchmark was a response time under 200ms even during peak load, did the solution meet this criterion? How well did the autoscaling work in response to increased load?
- **Assess Resource Usage**: Analyzed the resources used to implement and test the solution.
  - The team analyzes the resources used to implement and test the solution, comparing the actual usage against the estimates made during the Prediction phase. This could involve assessing the cost of running the Kubernetes cluster, the manpower required to set up and maintain it, etc.
- **Identify Issues**: Highlighted any problems or areas for improvement.
  - If the solution did not meet the benchmarks or if resource usage was higher than estimated, the team identifies these as issues. They also look for any other problems or areas for improvement, such as parts of the application that did not scale well, issues with the Kubernetes setup, etc.
- **Record Analysis**: Documented the analysis.
  - The team documents their analysis, including the comparison with benchmarks, resource usage assessment, and identified issues. This record provides a reference point for the next steps in the process and can be useful for future troubleshooting or similar projects.

## Iteration
- **Review Analysis**: Examined the analysis from the previous step.
  - The team examines the analysis from the previous step. This includes reviewing the comparison with benchmarks, the assessment of resource usage, and any identified issues.
- **Revise Hypothesis**: Updated the proposed solution based on the analysis.
  - Based on the analysis, the team updates the proposed solution. This could involve modifying the Kubernetes configuration to improve autoscaling, optimizing parts of the application to perform better under high load, or even considering other cloud-native technologies if Kubernetes did not meet their needs.
- **Consider Alternatives**: Explored other potential solutions if necessary.
  - If the revised hypothesis still doesn’t meet the benchmarks or if resource usage is still too high, the team explores other potential solutions. This could involve considering different autoscaling strategies, using different cloud-native technologies, or even rearchitecting the application to better suit a cloud-native environment.
- **Update Test Criteria**: Adjusted the test scenarios based on new hypothesis.
  - Based on the new hypothesis, the team adjusts the test scenarios. This could involve adding new test cases to cover areas that were problematic in the previous round of testing, modifying existing ones to better reflect the updated solution, or changing the expected outcomes based on the revised hypothesis.
- **Record Iteration**: Documented the iteration process.
  - The team documents the iteration process, including the revised hypothesis, any considered alternatives, and the updated test criteria. This record provides a reference point for the next steps in the process and can be useful for future troubleshooting or similar projects.

## Documentation
- **Summarize Findings**: Compiled a summary of the problem, solution, and results.
  - The team compiles a summary of the problem, the proposed solution, and the results of the testing. This provides a high-level overview of the project, which can be useful for reporting to stakeholders or for team members who were not deeply involved in the project.
- **Detail Process**: Described the steps taken, including any iterations and revisions.
  - The team describes the steps taken to solve the problem, including any iterations and revisions. This provides a clear record of the process and can be useful for future troubleshooting or for team members working on similar projects.
- **Record Metrics**: Documented the benchmarks, test results, and resource usage.
  - The team documents the benchmarks that were set, the results of the testing, and the resources used. This provides a quantitative record of the project and can help in assessing the effectiveness of the solution.
- **Note Issues**: Highlighted any remaining issues or areas for improvement.
  - The team highlights any remaining issues or areas for improvement. This can guide future work on the project.
- **Archive Documentation**: Stored the documentation for future reference.
  - The team stores the documentation in a place where it can be easily accessed for future reference. This ensures that the knowledge gained from the project is not lost and can benefit future work.
